### 1. Create a directory on the host machine that will persist data from the container
```
# for classic Jenkins
mkdir jenkins_data

# for jenkins with blue plugin
mkdir jenkins_blue_data

```
## 2. Decide on classic jenkins or jenkins Blue

### For classic jenkins
```
docker run -d --name jenkins -p 8080:8080 -p 50000:50000 -v $PWD/jenkins_data:/var/jenkins_home:z -t jenkins/jenkins:lts
```

### For Jenkins Blue
```
docker run -d --name jenkins_blue -p 8085:8080 -p 50000:50000 -v $PWD/jenkins_blue_data:/var/jenkins_home:z -t jenkinsci/blueocean
```

### If you are running docker on a remote server and want to access the application on your localhost you can perform whats called an ssh tunnel to forward traffic from the remote host(server) to your computer(localhost). 

### 3. Open a terminal session on your localhost

```
ssh -L 8085:127.0.0.1:8085 -N -f <remote_user>@<remote_ip>
```

### 4. Open a browser and go to http://localhost:8085
### 5. Obtain the password through the docker logs

### Obtain the container id by running `docker ps`. You should see output similar to the example below
```
docker ps

CONTAINER ID        IMAGE                 COMMAND                  CREATED             STATUS              PORTS                                              NAMES
de77da852ecd        jenkinsci/blueocean   "/sbin/tini -- /usr/…"   10 seconds ago      Up 9 seconds        0.0.0.0:50000->50000/tcp, 0.0.0.0:8085->8080/tcp   jenkins_blue
```

### 6. View the logs in order to obtain the access token needed to install the application
```
docker logs <CONTAINER ID>
```

### For example
```
$ *************************************************************
*************************************************************
*************************************************************

Jenkins initial setup is required. An admin user has been created and a password generated.
Please use the following password to proceed to installation:

fb5ceb48a84e460084adc238f2684a31

This may also be found at: /var/jenkins_home/secrets/initialAdminPassword

*************************************************************
*************************************************************
*************************************************************

```

### Paste `YOUR` token in the jenkins app running in browser at http://localhost:8085 and proceed with inital setup
